import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TOKEN, BASE_URL} from './url';
import Axios from 'axios';

const AddData = ({show, onClose}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  const postData = async () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const res = await ApiProvider.addDataMobil(body);
    console.log('response ADD : ', res);

    if (res.status === 200 || res.status === 201) {
      alert('Data Mobil berhasil ditambah');
    } else {
      alert('Gagal Tambah');
    }
  };

  return (
    <Modal visible={show} transparent={true}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.4)',
        }}>
        <View
          style={{
            width: 300,
            height: 450,
            backgroundColor: '#fff',
            justifyContent: 'center',
            borderRadius: 5,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
                marginLeft: 10,
                marginRight: 15,
              }}
              onPress={onClose}>
              <Icon name="times" size={20} color="#000" />
            </TouchableOpacity>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
              Tambah Data
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              padding: 15,
            }}>
            <View>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Nama Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Nama Mobil"
                style={styles.txtInput}
                onChangeText={text => setNamaMobil(text)}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Total Kilometer
              </Text>
              <TextInput
                placeholder="contoh: 100 KM"
                style={styles.txtInput}
                onChangeText={text => setTotalKM(text)}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Harga Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Harga Mobil"
                style={styles.txtInput}
                keyboardType="number-pad"
                onChangeText={text => setHargaMobil(text)}
              />
            </View>
            <TouchableOpacity style={styles.btnAdd} onPress={() => postData()}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                Tambah Data
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
